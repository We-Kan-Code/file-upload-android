# MVVM Boiler plate with ROOM/REALM

### This project is indented to describe the implementation and use of MVVM with Room and Realm

## Libraries used for Boiler plate

* Android X
* Lifecycle extension(viewmodel & livedata)
* Dagger 2
* Retrofit
* Moshi
* Testing framework(JUnit,Robolectic,Espresso,UIAutomator)
* Coroutine
* Glide
* Amazon S3

## Feature covered

1. Activity for demonstrating single file upload.
1. Service for demonstrating multiple file upload.
1. Unit test for Intent to other app to pick file using UIAutomator.
1. Unit test for stub intent with espresso