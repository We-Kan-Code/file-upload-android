package com.wk.fileupload.service

import android.app.Service
import android.content.Context
import androidx.test.core.app.ApplicationProvider
import com.wk.fileupload.App
import org.junit.After
import org.junit.Before
import org.junit.Test

import org.junit.Assert.*
import android.content.Intent
import android.util.Log
import androidx.test.espresso.IdlingRegistry
import androidx.test.espresso.intent.rule.IntentsTestRule
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.rule.ServiceTestRule
import com.amazonaws.mobileconnectors.s3.transfermanager.Download
import com.wk.fileupload.ui.dashboard.getPath
import com.wk.fileupload.utils.Constants
import org.junit.Rule
import androidx.test.uiautomator.By
import androidx.test.uiautomator.UiDevice
import com.amazonaws.mobile.client.AWSMobileClient
import com.amazonaws.mobile.client.Callback
import com.amazonaws.mobile.client.UserStateDetails
import java.util.concurrent.TimeUnit


class S3UploadServiceTest {
    @get:Rule
    val s3ServiceRule = ServiceTestRule()

    lateinit var appContext:Context
    @Before
    fun setUp() {
        appContext = ApplicationProvider.getApplicationContext<App>()
        //        Initialize the AWSMobileClient if not initialized
        AWSMobileClient.getInstance()
            .initialize(appContext, object : Callback<UserStateDetails> {
                override fun onResult(userStateDetails: UserStateDetails) {
                    Log.i("AWSMobileClient",
                        "AWSMobileClient initialized. User State is " + userStateDetails.userState
                    )
                }

                override fun onError(e: Exception) {
                    Log.e("AWSMobileClient", "Initialization error.", e)
                }
            })
    }

    @Test
    fun testUploadPartial(){
        IdlingRegistry.getInstance().register(S3UploadService.idleResource)

        var arrayUri = arrayListOf<String>()
        arrayUri.add("test1.jpg")
        arrayUri.add("test2.jpg")
        arrayUri.add("/storage/emulated/0/Download/21247.jpg")
        var serviceIntent = Intent(appContext, S3UploadService::class.java)
        serviceIntent.putStringArrayListExtra(Constants.UPLOAD_LIST,arrayUri)
        try {
            s3ServiceRule.startService(serviceIntent)
        }catch (e:Exception){

        }
        val device = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation())
        device.openNotification()
        val title = device.findObject(By.textContains("Uploading"))
        assertEquals("Uploading Partially Completed", title.text)
    }

    @Test
    fun testUploadCompleted(){

        var arrayUri = arrayListOf<String>()
        arrayUri.add("/storage/emulated/0/Download/lockscreen_06.jpg")
        arrayUri.add("/storage/emulated/0/Download/21832.jpg")
        arrayUri.add("/storage/emulated/0/Download/21247.jpg")
        var serviceIntent = Intent(appContext, S3UploadService::class.java)
        serviceIntent.putStringArrayListExtra(Constants.UPLOAD_LIST,arrayUri)
        try {
            s3ServiceRule.startService(serviceIntent)
        }catch (e:Exception){

        }
        IdlingRegistry.getInstance().register(S3UploadService.idleResource)

        val device = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation())
        device.openNotification()
        val title = device.findObject(By.textContains("Uploading"))
        assertEquals("Uploading Completed", title.text)
    }

    @Test
    fun testUploadFailed(){
        IdlingRegistry.getInstance().register(S3UploadService.idleResource)

        var arrayUri = arrayListOf<String>()
        arrayUri.add("lockscreen_06.jpg")
        arrayUri.add("21832.jpg")
        arrayUri.add("Download/21247.jpg")
        var serviceIntent = Intent(appContext, S3UploadService::class.java)
        serviceIntent.putStringArrayListExtra(Constants.UPLOAD_LIST,arrayUri)
        try {
            s3ServiceRule.startService(serviceIntent)
        }catch (e:Exception){

        }
        val device = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation())
        device.openNotification()
        val title = device.findObject(By.textContains("Uploading"))
        assertEquals("Uploading Failed", title.text)
    }

    @After
    fun testStop() {
        IdlingRegistry.getInstance().unregister(S3UploadService.idleResource)
    }
}