package com.wk.fileupload.ui.dashboard

import android.content.Intent
import android.util.Log
import androidx.test.core.app.ApplicationProvider
import androidx.test.espresso.Espresso
import androidx.test.espresso.IdlingRegistry
import androidx.test.espresso.ViewInteraction
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.rule.ActivityTestRule
import androidx.test.rule.ServiceTestRule
import com.amazonaws.mobile.client.AWSMobileClient
import com.amazonaws.mobile.client.Callback
import com.amazonaws.mobile.client.UserStateDetails
import com.wk.fileupload.App
import com.wk.fileupload.R
import com.wk.fileupload.service.S3UploadService
import com.wk.fileupload.utils.Constants
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import android.app.Activity
import android.app.Instrumentation.ActivityResult
import androidx.test.espresso.intent.Intents.intending
import android.os.Parcelable
import android.os.Bundle
import android.app.Instrumentation
import android.content.ContentResolver
import android.graphics.Rect
import android.net.Uri
import androidx.test.espresso.intent.Intents
import androidx.test.espresso.intent.Intents.intended
import androidx.test.espresso.intent.matcher.IntentMatchers
import androidx.test.espresso.intent.matcher.IntentMatchers.*
import androidx.test.espresso.intent.rule.IntentsTestRule
import androidx.test.rule.GrantPermissionRule
import androidx.test.rule.provider.ProviderTestRule
import androidx.test.uiautomator.*
import org.hamcrest.core.AllOf
import java.io.File


class DashboardActivityTest{
    @get:Rule
    val activityDashRule = IntentsTestRule(DashboardActivity::class.java)
    lateinit var uploadSingle : ViewInteraction
    lateinit var startService : ViewInteraction
    lateinit var stopService : ViewInteraction
    lateinit var uploadProgressBar : ViewInteraction

    @get:Rule var permissionRule = GrantPermissionRule.grant(android.Manifest.permission.READ_EXTERNAL_STORAGE)

    @Before
    fun setUp() {
        uploadSingle = Espresso.onView(ViewMatchers.withId(com.wk.fileupload.R.id.updateProfilePic))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        startService = Espresso.onView(ViewMatchers.withId(com.wk.fileupload.R.id.start))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        stopService = Espresso.onView(ViewMatchers.withId(com.wk.fileupload.R.id.stop))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        uploadProgressBar = Espresso.onView(ViewMatchers.withId(com.wk.fileupload.R.id.upload_progress_bar))

        val resultData = Intent()
        val uri1 = Uri.fromFile(File("/storage/emulated/0/Download/21832.jpg"))
        // Create the Intent that will include the bundle.
        resultData.setData(uri1)
        intending(AllOf.allOf(hasAction(Intent.ACTION_GET_CONTENT), hasExtra(Intent.EXTRA_ALLOW_MULTIPLE,false))).respondWith(
            Instrumentation.ActivityResult(
                Activity.RESULT_OK,
                resultData
            )
        )
    }

    @Test
    fun testUploadMockPickSingleFile(){
        IdlingRegistry.getInstance().register(S3UploadService.idleResource)
        val device = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation())

        uploadSingle.perform(ViewActions.click())
        intended(AllOf.allOf(hasAction(Intent.ACTION_GET_CONTENT),hasExtra(Intent.EXTRA_ALLOW_MULTIPLE,false)))
        uploadProgressBar.check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        val title = device.findObject(By.textContains("Uploading"))
        assertEquals("Uploading Completed", title.text)
    }

    @Test
    fun testUploadPickSingleFile(){
        IdlingRegistry.getInstance().register(S3UploadService.idleResource)

        uploadSingle.perform(ViewActions.click())
        intended(AllOf.allOf(hasAction(Intent.ACTION_GET_CONTENT),hasExtra(Intent.EXTRA_ALLOW_MULTIPLE,false)))
        uploadProgressBar.check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
    }

    @Test
    fun testUIAutoUploadPickMultiFile(){
        IdlingRegistry.getInstance().register(S3UploadService.idleResource)

        startService.perform(ViewActions.click())
        intended(AllOf.allOf(hasAction(Intent.ACTION_GET_CONTENT), hasExtra(Intent.EXTRA_ALLOW_MULTIPLE,true)))
        val device = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation())

        var child = device.findObject( UiSelector()
            .className("android.widget.TextView").textContains("lockscreen"))
        var child_rect= child .getBounds()
        device.swipe(child_rect.centerX(), child_rect.centerY(), child_rect.centerX(), child_rect.centerY(), 100)
        var child1 = device.findObject( UiSelector()
            .className("android.widget.TextView").textContains("218"))
        child1.clickAndWaitForNewWindow()
//        var menuButton = device.findObject(By.text("Open"))
        var done = device.findObject( UiSelector().textContains("Open"))
        done.click()

        device.openNotification()
        val title = device.findObject(By.textContains("Uploading"))
        assertEquals("Uploading Completed", title.text)

    }
}