package com.wk.fileupload.service

import android.app.Service
import android.content.Intent
import android.os.Build
import android.os.IBinder
import android.util.Log
import androidx.core.app.NotificationCompat
import com.wk.fileupload.utils.AppUtils
import java.lang.Exception
import android.app.NotificationManager
import android.content.Context
import com.amazonaws.mobileconnectors.s3.transferutility.*
import com.wk.fileupload.utils.Constants
import java.io.File
import java.util.*
import kotlin.collections.ArrayList
import androidx.annotation.RestrictTo
import androidx.test.espresso.idling.CountingIdlingResource

class S3UploadService : Service() {

    companion object{
        @RestrictTo(RestrictTo.Scope.TESTS)
        var idleResource = CountingIdlingResource("s3service")

        @RestrictTo(RestrictTo.Scope.TESTS)
        private var isServiceStarted: Boolean = false

        @RestrictTo(RestrictTo.Scope.TESTS)
        fun isServiceStarted(): Boolean {
            return isServiceStarted
        }
    }
    lateinit var mNotificationManager:NotificationManager
    lateinit var notification: NotificationCompat.Builder
    lateinit var tsIntent:Intent

    override fun onCreate() {
        super.onCreate()

        @RestrictTo(RestrictTo.Scope.TESTS)
        isServiceStarted = true

        Log.i(this.javaClass.name.toString(),"onCreate")

        mNotificationManager =
            getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationBuilder = AppUtils.createAppNotification(this,getString(com.wk.fileupload.R.string.uploaping),getString(
            com.wk.fileupload.R.string.prep_uploaping))
        notificationBuilder.setOnlyAlertOnce(true)
        notification = AppUtils.createAppNotification(this,getString(com.wk.fileupload.R.string.uploaping),"")
        // initlizing intent for transfer service
        tsIntent = Intent(applicationContext, TransferService::class.java)
        tsIntent.putExtra(TransferService.INTENT_KEY_NOTIFICATION, notification.build())
        tsIntent.putExtra(TransferService.INTENT_KEY_NOTIFICATION_ID, AppUtils.S3_NOTIFICATION_ID)
        tsIntent.putExtra(TransferService.INTENT_KEY_REMOVE_NOTIFICATION, true)

    }
    var s3PathList = arrayListOf<String>()
    var transferUtility: TransferUtility? = null
    /**
     * initializing tranfer util for upload
     */
    fun initTransferUtil(context:Context){
        // starting transfer service
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            applicationContext.startForegroundService(tsIntent)
        }else{
            applicationContext.startService(tsIntent)
        }
        transferUtility = AppUtils.createTranferUtil(this)
    }

    var fileCount = 0
    /**
     *  handle multiple file upload
     */
    fun uploadMultipleWithTransferUtility(filePathList:ArrayList<String>){
        fail = 0
        s3PathList.clear()

        idleResource.increment()
        fileCount =+ filePathList.size
        filePathList.forEach{
            uploadWithTransferUtility(it)
        }
    }
    var fail = 0
    /**
     *  update notification for complete / failure of upload
     */
    fun completedUploadNotification(content:String,progress:Int){
        if(notification!=null) {
            mNotificationManager =
                getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notification.setContentTitle(content)
            notification.setContentText(getString(com.wk.fileupload.R.string.upload_progress, progress, fileCount))
            notification.setOngoing(false)
            if(transferUtility!=null)
                applicationContext.stopService(tsIntent)
            mNotificationManager.notify(
                AppUtils.S3_NOTIFICATION_ID,
                notification.build()
            )
        }
        fileCount = 0
    }

    /**
     * initalizing upload by transfer util and setting listener on the upload observer
     */
    fun uploadWithTransferUtility(filePath:String) {
        try{
            val uploadFile = File(filePath)
            if(uploadFile.exists()) {
                if(transferUtility==null){
                    initTransferUtil(this)
                }
                val uploadPath = "public/${Date().time}.${uploadFile.extension}"
                Log.i("upload", "$filePath $uploadPath")
                val uploadObserver = transferUtility?.upload(
                    uploadPath,
                    uploadFile
                )

                // Attach a listener to the observer to get state update and progress notifications
                uploadObserver?.setTransferListener(object : TransferListener {

                    override fun onStateChanged(id: Int, state: TransferState) {
                        if(!s3PathList.contains(uploadPath)) {
                            if (TransferState.COMPLETED === state) {
                                // Handle a completed upload.
                                s3PathList.add(uploadPath)
                                Log.i(
                                    "completed",
                                    "${uploadPath} ${s3PathList.size} ${fileCount} ${fail}"
                                )
                                if (s3PathList.size == fileCount) {
                                    completedUploadNotification(
                                        getString(com.wk.fileupload.R.string.uploap_completed),
                                        s3PathList.size
                                    )
                                    idleResource.decrement()
                                } else if (s3PathList.size > 0 && fail > 0 && (fail + s3PathList.size) == fileCount) {
                                    completedUploadNotification(
                                        getString(com.wk.fileupload.R.string.uploap_partial),
                                        s3PathList.size
                                    )
                                    idleResource.decrement()
                                } else if (fail > 0 && fail == fileCount) {
                                    completedUploadNotification(
                                        getString(com.wk.fileupload.R.string.uploap_failed),
                                        fail
                                    )
                                    idleResource.decrement()
                                } else {
                                    updateNotification(
                                        content = getString(
                                            com.wk.fileupload.R.string.upload_progress,
                                            s3PathList.size,
                                            fileCount
                                        )
                                    )
                                }
                            } else if (TransferState.FAILED === state) {
                                s3PathList.add(uploadPath)
                                fail++
                                if (s3PathList.size > 0 && (fail + s3PathList.size) == fileCount) {
                                    completedUploadNotification(
                                        getString(com.wk.fileupload.R.string.uploap_partial),
                                        s3PathList.size
                                    )
                                    idleResource.decrement()
                                } else if (fail == fileCount) {
                                    completedUploadNotification(
                                        getString(com.wk.fileupload.R.string.uploap_failed),
                                        fail
                                    )
                                    idleResource.decrement()
                                }
                            }
                        }
                    }

                    override fun onProgressChanged(id: Int, bytesCurrent: Long, bytesTotal: Long) {
                        val percentDonef = bytesCurrent.toFloat() / bytesTotal.toFloat() * 100
                        val percentDone = percentDonef.toInt()
                        updateNotification(
                            content = getString(
                                com.wk.fileupload.R.string.upload_progress,
                                s3PathList.size,
                                fileCount
                            )
                        )
                        Log.d(
                            "YourActivity", "ID:" + id + " bytesCurrent: " + bytesCurrent
                                    + " bytesTotal: " + bytesTotal + " " + percentDone + "%"
                        )
                    }

                    override fun onError(id: Int, ex: Exception) {
                        // Handle errors
                        s3PathList.add(uploadPath)
                        fail++
                        if(s3PathList.size > 0 && (fail + s3PathList.size)  == fileCount)
                        {
                            completedUploadNotification(getString(com.wk.fileupload.R.string.uploap_partial),s3PathList.size)
                            idleResource.decrement()
                        }else if(fail  == fileCount)
                        {
                            completedUploadNotification(getString(com.wk.fileupload.R.string.uploap_failed),fail)
                            idleResource.decrement()
                        }
                    }

                })
            }else{
                fail++
                if(fail  == fileCount)
                {
                    completedUploadNotification(getString(com.wk.fileupload.R.string.uploap_failed),fail)
                    idleResource.decrement()
                }
            }
        }catch (e:Exception){
            e.printStackTrace()
            fail++
            if(fail  == fileCount)
            {
                completedUploadNotification(getString(com.wk.fileupload.R.string.uploap_failed),fail)
                idleResource.decrement()
            }
        }
    }

    lateinit var notificationBuilder : NotificationCompat.Builder

    /**
     * handling the intent for multiple files upload
     */
    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {

        Log.i(this.javaClass.name.toString(),"onStartCommand")
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
//            startForeground(AppUtils.S3_NOTIFICATION_ID, notificationBuilder.build())
//        }
        try {
            if(intent != null && intent.hasExtra(Constants.UPLOAD_LIST)) {
                var arrayUril = intent.getStringArrayListExtra(Constants.UPLOAD_LIST)
                uploadMultipleWithTransferUtility(arrayUril)
//                GlobalScope.launch {
//                    while (current_index <= size) {
//                        Log.i("delay", "before")
//                        delay(5000)
//                        Log.i("delay", "after")
//                        updateNotification(
//                            content = getString(
//                                R.string.upload_progress,
//                                current_index,
//                                size
//                            )
//                        )
//                        current_index++
//                    }
//                    updateNotification(
//                        getString(R.string.uploap_completed),
//                        getString(R.string.upload_progress, current_index, size)
//                    )
//                    stopForeground(false)
//                    current_index = 1
//                }
            }
        }catch (e:Exception){
            e.printStackTrace()
        }

        return START_STICKY
    }

    /**
     * updating notification for changes
     */
    fun updateNotification(title:String = "",content:String){
        if(title.isNotEmpty())
            notificationBuilder.setContentTitle(title)
        notificationBuilder.setContentText(content)
        mNotificationManager.notify(AppUtils.S3_NOTIFICATION_ID, notificationBuilder.build())
    }

    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

    /**
     * canceling the uploads on service destroy
     */
    override fun onDestroy() {
        @RestrictTo(RestrictTo.Scope.TESTS)
        isServiceStarted = false
        if(transferUtility?.getTransfersWithType(TransferType.UPLOAD)?.size!!>0)
            transferUtility?.cancelAllWithType(TransferType.UPLOAD)
        super.onDestroy()
    }
}