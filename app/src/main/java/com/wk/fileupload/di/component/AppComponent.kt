package com.wk.fileupload.di.component

import com.wk.fileupload.App
import com.wk.fileupload.di.module.ActivityBuildersModule
import com.wk.fileupload.di.module.AppModule
import com.wk.fileupload.di.module.ServiceModule
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

/**
 * class to define the components that are provided for the app with the defined list of modules
 */
@Singleton
@Component(
    modules = [(AndroidInjectionModule::class),(AppModule::class),(ActivityBuildersModule::class),(ServiceModule::class)]
)
interface AppComponent {
    fun inject(app: App)
}