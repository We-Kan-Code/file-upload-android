package com.wk.fileupload.di.module

import com.wk.fileupload.service.S3UploadService
import dagger.Module
import dagger.android.ContributesAndroidInjector


@Module
abstract class ServiceModule {

    @ContributesAndroidInjector
    abstract fun contributeS3UploadService(): S3UploadService
}