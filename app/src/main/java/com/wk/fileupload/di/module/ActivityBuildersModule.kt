package com.wk.fileupload.di.module

import com.wk.fileupload.ui.dashboard.DashboardActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * class to provide the injection for the activities
 */
@Module
abstract class ActivityBuildersModule {

    @ContributesAndroidInjector
    abstract fun contributeDashboardActivity(): DashboardActivity

}