package com.wk.fileupload.ui.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.view.menu.ActionMenuItemView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.wk.fileupload.BuildConfig
import com.wk.fileupload.R
import kotlinx.android.synthetic.main.fragment_dashboard.*
import kotlin.coroutines.coroutineContext

class DashboardImageAdapter(val data:ArrayList<String>,val context:Context) : RecyclerView.Adapter<DashboardImageAdapter.ViewHolder>() {
    private var options:RequestOptions = RequestOptions()
        .centerCrop()
        .placeholder(R.mipmap.ic_launcher_round)
        .error(R.mipmap.ic_launcher_round)

    override fun getItemCount(): Int {
        return data.size
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item, parent, false)
        return ViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val pictureUrl = data[position]
        Glide.with(context).load("${BuildConfig.S3_BASE_URL}${pictureUrl}").apply(options)
            .into(holder.picture)
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        var picture:ImageView = itemView.findViewById(R.id.picture)
    }
}