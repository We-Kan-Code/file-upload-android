package com.wk.fileupload.ui.dashboard

import android.Manifest
import android.app.Activity
import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.database.Cursor
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.os.StrictMode
import android.provider.DocumentsContract
import android.provider.MediaStore
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.NotificationCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.wk.fileupload.R
import com.wk.fileupload.ui.adapters.DashboardImageAdapter
import dagger.android.AndroidInjection

import kotlinx.android.synthetic.main.activity_dashboard.*
import kotlinx.android.synthetic.main.fragment_dashboard.*
import javax.inject.Inject
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.amazonaws.mobile.client.AWSMobileClient
import com.amazonaws.mobile.client.Callback
import com.amazonaws.mobile.client.UserStateDetails
import com.amazonaws.mobileconnectors.s3.transferutility.TransferService
import com.wk.fileupload.BuildConfig
import com.wk.fileupload.service.S3UploadService
import com.wk.fileupload.ui.base.BaseActivity
import com.wk.fileupload.ui.base.PermissionRequestListener
import com.wk.fileupload.utils.AppUtils
import com.wk.fileupload.utils.Constants.Companion.UPLOAD_LIST
import com.wk.fileupload.utils.Constants.Companion.UPLOAD_REQUEST_CODE
import kotlinx.android.synthetic.main.fragment_dashboard.profile_pic
import java.io.File
import java.net.URISyntaxException


/**
 * Activity for dashboard or landing page after successful login
 */
class DashboardActivity : BaseActivity(), PermissionRequestListener {
    //initializing dashboard view model factory
    @Inject
    lateinit var dashboardViewModelFactory:DashboardViewModelFactory

    lateinit var dashBoardViewModel: DashBoardViewModel
    var allPermissionGranted = false
    private lateinit var permissions : ArrayList<String>

    lateinit var mNotificationManager: NotificationManager
    lateinit var notification: NotificationCompat.Builder
    lateinit var tsIntent:Intent

    var ALLOW_MULTIPLE = true
    lateinit var s3UploadService:Intent

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashboard)
        setSupportActionBar(toolbar)

        AndroidInjection.inject(this)

        permissionListener = this
//        Initialize the AWSMobileClient if not initialized
        AWSMobileClient.getInstance()
            .initialize(applicationContext, object : Callback<UserStateDetails> {
                override fun onResult(userStateDetails: UserStateDetails) {
                    Log.i("AWSMobileClient",
                        "AWSMobileClient initialized. User State is " + userStateDetails.userState
                    )
                }

                override fun onError(e: Exception) {
                    Log.e("AWSMobileClient", "Initialization error.", e)
                }
            })
        //initializing dashboard view model
        dashBoardViewModel = ViewModelProviders.of(this,dashboardViewModelFactory).get(DashBoardViewModel::class.java)

        //observing the viewmodel for dashboard api result change
        dashBoardViewModel.uploadResult.observe(this@DashboardActivity, Observer{
            val dashResult = it ?: return@Observer

            if (dashResult.error != null) {
                upload_progress_bar.visibility = View.GONE
                showToast(dashResult.error)
                upload_progress_bar.visibility = View.GONE
                completedUploadNotification(getString(R.string.uploap_failed))
            }
            if (dashResult.errorCode != null) {
                upload_progress_bar.visibility = View.GONE
                showToast(dashResult.errorCode)
                upload_progress_bar.visibility = View.GONE
                completedUploadNotification(getString(R.string.uploap_failed))
            }
            if (dashResult.uploadedFile != null) {
                upload_progress_bar.visibility = View.GONE
                var options = RequestOptions()
                    .centerCrop()
                    .placeholder(R.mipmap.ic_launcher)
                    .error(R.mipmap.ic_launcher)
                Glide.with(this).load("${BuildConfig.S3_BASE_URL}${dashResult.uploadedFile}").apply(options)
                    .into(profile_pic)
                showToast(com.wk.fileupload.R.string.upload_success)
                completedUploadNotification(getString(R.string.uploap_completed))
            }
            if (dashResult.uploadProgress != null) {
                upload_progress_bar.progress = dashResult.uploadProgress
            }
            if (dashResult.uploadedFileList != null) {
                upload_progress_bar.visibility = View.GONE
                var usersAdapter = DashboardImageAdapter(dashResult.uploadedFileList,this)
                picture_list.adapter = usersAdapter
                picture_list.setLayoutManager(LinearLayoutManager(this))
                picture_list.setHasFixedSize(true)
                picture_list.addItemDecoration(
                    DividerItemDecoration(
                        this,
                        DividerItemDecoration.VERTICAL
                    )
                )
                completedUploadNotification(getString(R.string.uploap_completed))
            }
        })

        s3UploadService = Intent(this,S3UploadService::class.java)

        start.setOnClickListener {
            ALLOW_MULTIPLE = true
            checkPermission4Pick()
        }
        stop.setOnClickListener {
            stopService(s3UploadService)
        }

        updateProfilePic.setOnClickListener{
            ALLOW_MULTIPLE = false
            checkPermission4Pick()
        }
    }

    override fun onResume() {
        super.onResume()
    }

    /**
     * show upload complete/failed notification
     */
    fun completedUploadNotification(content:String){
        if(notification!=null) {
            mNotificationManager =
                getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notification.setContentTitle(content)
            notification.setContentText(getString(R.string.upload_progress, 1, 1))
            notification.setOngoing(false)
            mNotificationManager.notify(
                AppUtils.S3_NOTIFICATION_ID,
                notification.build()
            )
        }
    }
    /**
     * listener for permission resquest
     */
    override fun permissionResponse(permissions: Array<out String>, grantResults: IntArray) {
        for (i in grantResults.indices) {
            allPermissionGranted = grantResults[i] == PackageManager.PERMISSION_GRANTED
            if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                // user rejected the permission
                var showRationale = shouldShowRequestPermissionRationale(permissions[i])
                if (!showRationale) {
                    //execute when 'never Ask Again' tick and permission dialog not show
                    Toast.makeText(this,R.string.allow_storage_permission_settings, Toast.LENGTH_SHORT).show()
                } else {
                    Toast.makeText(this,R.string.allow_storage_permission, Toast.LENGTH_SHORT).show()
                }
                allPermissionGranted = false
                break
            }
        }

        if(allPermissionGranted){
            pickProfilePicture()
        }

    }

    /**
     * check for permission and request if not granted
     */
    fun checkPermission4Pick(){
        permissions = arrayListOf()
        if(checkPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)){
            permissions.add(Manifest.permission.WRITE_EXTERNAL_STORAGE)
        }

        if(checkPermission(Manifest.permission.READ_EXTERNAL_STORAGE)){
            permissions.add(Manifest.permission.READ_EXTERNAL_STORAGE)
        }

        if(permissions.isNotEmpty()){
            requestPermission(permissions.toTypedArray())
        }else {
            pickProfilePicture()
        }
    }
    /**
     * pick up file from the device using other application
     */
    private fun pickProfilePicture(){
        var galleryIntent =
            Intent(Intent.ACTION_GET_CONTENT)//MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        galleryIntent.type = "image/*"
        galleryIntent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, ALLOW_MULTIPLE)
        var packageManager = this.packageManager
        if (galleryIntent.resolveActivity(packageManager) != null) {
            startActivityForResult(galleryIntent, UPLOAD_REQUEST_CODE)
        } else {
            Log.d("pickProfilePicture", "No application available to handle action");
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == UPLOAD_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                val uri = data?.data
                Log.e("onActivityResult", uri.toString())
                if(uri!=null) {// single file picked
                    notification = AppUtils.createAppNotification(this,getString(R.string.uploaping),getString(R.string.upload_progress,1,1))
                    tsIntent = Intent(applicationContext, TransferService::class.java)
                    tsIntent.putExtra(TransferService.INTENT_KEY_NOTIFICATION, notification.build())
                    tsIntent.putExtra(TransferService.INTENT_KEY_NOTIFICATION_ID, AppUtils.S3_NOTIFICATION_ID)
                    tsIntent.putExtra(TransferService.INTENT_KEY_REMOVE_NOTIFICATION, true)
                    try {
                        val path = getPath(this, uri)
                        Log.e("onActivityResult", path)
                        if(File(path).exists()) {
                            dashBoardViewModel.initTransferUtil(AppUtils.createTranferUtil(this))
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                                applicationContext.startForegroundService(tsIntent)
                            } else {
                                applicationContext.startService(tsIntent)
                            }
                            upload_progress_bar.visibility = View.VISIBLE
                            dashBoardViewModel.MULTIPLEUPLOAD = false
                            dashBoardViewModel.uploadWithTransferUtility(path!!)
                        }else{
                            completedUploadNotification(getString(R.string.uploap_failed))
                        }
                    } catch (e: URISyntaxException) {

                        upload_progress_bar.visibility = View.GONE
                        Toast.makeText(
                            this,
                            "Unable to get the file from the given URI. See error log for details",
                            Toast.LENGTH_LONG
                        ).show()
                        Log.e("onActivityResult", "Unable to upload file from the given uri", e)
                    }
                }else if(data?.clipData != null){// mulitple files picked
                    var arrayUri = arrayListOf<String>()
                    var clipdata = data?.clipData
                    Log.e("onActivityResult", "${clipdata.itemCount}")
                    try {

                        for (i in 0 until clipdata.itemCount){
                            var path = getPath(this, clipdata.getItemAt(i).uri)
                            if(path!=null){
                                arrayUri.add(path)
                                Log.e("onActivityResult", "${path}")
                            }
                        }
                        s3UploadService.putStringArrayListExtra(UPLOAD_LIST,arrayUri)
                        startService(s3UploadService)
                    } catch (e: URISyntaxException) {

                        upload_progress_bar.visibility = View.GONE
                        Toast.makeText(
                            this,
                            "Unable to get the file from the given URI. See error log for details",
                            Toast.LENGTH_LONG
                        ).show()
                        Log.e("onActivityResult", "Unable to upload file from the given uri", e)
                    }
                }

            }
        }
    }

}

private val PUBLIC_DOWNLOAD_PATH = "content://downloads/public_downloads"
private val EXTERNAL_STORAGE_DOCUMENTS_PATH = "com.android.externalstorage.documents"
private val DOWNLOAD_DOCUMENTS_PATH = "com.android.providers.downloads.documents"
private val MEDIA_DOCUMENTS_PATH = "com.android.providers.media.documents"
private val PHOTO_CONTENTS_PATH = "com.google.android.apps.photos.content"

//HELPER METHODS
private fun isExternalStorageDocument(uri: Uri): Boolean {
    return EXTERNAL_STORAGE_DOCUMENTS_PATH == uri.authority
}
private fun isDownloadsDocument(uri: Uri): Boolean {
    return DOWNLOAD_DOCUMENTS_PATH == uri.authority
}
private fun isMediaDocument(uri: Uri): Boolean {
    return MEDIA_DOCUMENTS_PATH == uri.authority
}
private fun isGooglePhotosUri(uri: Uri): Boolean {
    return PHOTO_CONTENTS_PATH == uri.authority
}


/**
 * Extract the file path from uri
 */
fun getPath(context: Context, uri: Uri): String? {
    if (DocumentsContract.isDocumentUri(context, uri)) {
        if (isExternalStorageDocument(uri)) {
            Log.i("test","1")
            val docId = DocumentsContract.getDocumentId(uri)
            val split = docId.split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
            val type = split[0]
            val storageDefinition: String
            if ("primary".equals(type, ignoreCase = true)) {
                return Environment.getExternalStorageDirectory().toString() + "/" + split[1]
            } else {
                if (Environment.isExternalStorageRemovable()) {
                    storageDefinition = "EXTERNAL_STORAGE"
                } else {
                    storageDefinition = "SECONDARY_STORAGE"
                }
                return System.getenv(storageDefinition) + "/" + split[1]
            }
        } else if (isDownloadsDocument(uri)) {
            Log.i("test","2")
            //val id = DocumentsContract.getDocumentId(uri) //MAY HAVE TO USE FOR OLDER PHONES, HAVE TO TEST WITH REGRESSION MODELS
            //val contentUri = ContentUris.withAppendedId(Uri.parse(PUBLIC_DOWNLOAD_PATH), id.toLong()) //SAME NOTE AS ABOVE
            val fileName = getDataColumn(context, uri, null, null)
            var uriToReturn: String? = null
            if(fileName != null){
                Log.i("test","22")
                uriToReturn = Uri.withAppendedPath(
                    Uri.parse(
                        Environment.getExternalStoragePublicDirectory(
                            Environment.DIRECTORY_DOWNLOADS).absolutePath), fileName).toString()
            }
            return uriToReturn
        } else if (isMediaDocument(uri)) {
            Log.i("test","3")
            val docId = DocumentsContract.getDocumentId(uri)
            val split = docId.split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
            val type = split[0]
            var contentUri: Uri? = null
            if ("image" == type) {
                contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI
            } else if ("video" == type) {
                contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI
            } else if ("audio" == type) {
                contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI
            }
            val selection = "_id=?"
            val selectionArgs = arrayOf(split[1])
            return getDataColumn(context, contentUri!!, selection, selectionArgs)
        }
    } else if ("content".equals(uri.scheme, ignoreCase = true)) {
        return if (isGooglePhotosUri(uri)) uri.lastPathSegment else getDataColumn(context, uri, null, null)
    } else if ("file".equals(uri.scheme, true)) {
        Log.i("test","4")
        return uri.path
    }
    return null
}



/**
 * Extract the file path from uri thru content resolver
 */
private fun getDataColumn(context: Context, uri: Uri, selection: String?, selectionArgs: Array<String>?): String? {
    var cursor: Cursor? = null
    //val column = "_data" REMOVED IN FAVOR OF NULL FOR ALL
    //val projection = arrayOf(column) REMOVED IN FAVOR OF PROJECTION FOR ALL
    try {
        Log.i("test","23")
        cursor = context.contentResolver.query(uri, null, selection, selectionArgs, null)
        if (cursor != null && cursor.moveToFirst()) {
            Log.i("test","24")
            val columnIndex = cursor.getColumnIndexOrThrow(DocumentsContract.Document.COLUMN_DISPLAY_NAME) //_display_name
            return cursor.getString(columnIndex) //returns file name
        }
    }catch (ex: Exception){
        Log.e("PathUtils", "Error getting uri for cursor to read file: ${ex.message}")
    } finally {
        if (cursor != null)
            cursor.close()
    }
    return null
}