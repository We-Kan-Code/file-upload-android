package com.wk.fileupload.ui.dashboard

/**
 * Data validation for load user Api of the dashboard.
 */

data class UploadResult(
    val uploadProgress: Int? = null,
    val uploadedFile: String? = null,
    val uploadedFileList: ArrayList<String>? = null,
    val errorCode: Int? = null,
    val error: String? = null
)
