package com.wk.fileupload.ui.dashboard

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import javax.inject.Inject

/**
 * ViewModel provider factory to instantiate LoginViewModel.
 * Required given LoginViewModel has a non-empty constructor
 */
class DashboardViewModelFactory @Inject constructor(private val dashboardViewModel: DashBoardViewModel): ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(DashBoardViewModel::class.java)) {
            return dashboardViewModel as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}
