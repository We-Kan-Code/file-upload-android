package com.wk.fileupload.ui.dashboard

import android.content.Context
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.test.espresso.idling.CountingIdlingResource
import com.amazonaws.mobile.client.AWSMobileClient
import com.amazonaws.mobileconnectors.s3.transferutility.*
import com.amazonaws.services.s3.AmazonS3Client
import com.wk.fileupload.R
import com.wk.fileupload.ui.login.BaseViewModel
import com.wk.fileupload.utils.AppUtils
import java.io.File
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList

/**
 * View model class for dashboard
 */
class DashBoardViewModel @Inject constructor(): BaseViewModel() {
    var idleResource = CountingIdlingResource("dash")
    /**
     * livedata to observe upload result data
     */
    private val _uploadResult = MutableLiveData<UploadResult>()
    val uploadResult: LiveData<UploadResult> = _uploadResult


    var transferUtility:TransferUtility? = null
    fun initTransferUtil(cTransferUtility:TransferUtility){
        transferUtility = cTransferUtility
    }

    var MULTIPLEUPLOAD = false
    var s3PathList = arrayListOf<String>()
    var fileCount = 0
    fun uploadMultipleWithTransferUtility(filePathList:ArrayList<String>){
        fileCount = filePathList.size
        filePathList.forEach{

            uploadWithTransferUtility(it)
        }
    }

    fun uploadWithTransferUtility(filePath:String) {
        idleResource.increment()
        try{
            val uploadFile = File(filePath)
            if(uploadFile.exists()) {
                val uploadPath = "public/${Date().time}.${uploadFile.extension}"
                Log.i("upload", "$filePath $uploadPath.${uploadFile.extension}")
                val uploadObserver = transferUtility?.upload(
                    uploadPath,
                    uploadFile
                )

                // Attach a listener to the observer to get state update and progress notifications
                uploadObserver?.setTransferListener(object : TransferListener {

                    override fun onStateChanged(id: Int, state: TransferState) {
                        if (TransferState.COMPLETED === state) {
                            // Handle a completed upload.
                            if (!MULTIPLEUPLOAD)
                                _uploadResult.value =
                                    UploadResult(100, uploadPath, null, null, null)
                            else {
                                s3PathList.add(uploadPath)
                                if (s3PathList.size == fileCount)
                                    _uploadResult.value =
                                        UploadResult(100, null, s3PathList, null, null)
                            }
                            idleResource.decrement()
                        }else if (TransferState.FAILED === state){
                            idleResource.decrement()
                        }
                    }

                    override fun onProgressChanged(id: Int, bytesCurrent: Long, bytesTotal: Long) {
                        val percentDonef = bytesCurrent.toFloat() / bytesTotal.toFloat() * 100
                        val percentDone = percentDonef.toInt()
                        _uploadResult.value = UploadResult(percentDone, null, null, null, null)

                        Log.d(
                            "YourActivity", "ID:" + id + " bytesCurrent: " + bytesCurrent
                                    + " bytesTotal: " + bytesTotal + " " + percentDone + "%"
                        )
                    }

                    override fun onError(id: Int, ex: Exception) {
                        // Handle errors
                        _uploadResult.value = UploadResult(errorCode = R.string.unknown_error)
                        idleResource.decrement()
                    }

                })
            }else{
                _uploadResult.value = UploadResult(errorCode = R.string.file_error)
                idleResource.decrement()
            }
        }catch (e:Exception){
            e.printStackTrace()
            _uploadResult.value = UploadResult(errorCode = R.string.unknown_error)
            idleResource.decrement()
        }
    }

    override fun onCleared() {
        transferUtility?.cancelAllWithType(TransferType.UPLOAD)
        super.onCleared()
    }
}