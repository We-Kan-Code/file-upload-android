package com.wk.fileupload

import android.app.Activity
import android.app.Application
import com.wk.fileupload.di.component.DaggerAppComponent
import com.wk.fileupload.di.module.AppModule
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import javax.inject.Inject


/**
 * Application class to initialize sdk's and other lib/service that were supposed to be initialize on application class
 */
class App : Application(), HasAndroidInjector {

    val TAG = App::class.java.name

    @Inject
    lateinit var activityInjector: DispatchingAndroidInjector<Activity>


    override fun onCreate() {
        super.onCreate()
        DaggerAppComponent.builder().appModule(AppModule(this)).build().inject(this)

    }

    override fun androidInjector(): AndroidInjector<Any> = activityInjector as AndroidInjector<Any>

}