package com.wk.fileupload.utils

/**
 * Constants for app/intent keys
 */
class Constants{
    companion object{
        const val PERMISSION_REQUSET_CODE = 200
        const val UPLOAD_REQUEST_CODE = 300
        const val UPLOAD_LIST = "uploadlist"
    }
}

/**
 * Constants for shared preferences key
 */
class PrefKeys {
    companion object{
        val PREFNAME = "app_pref"
        val USERDETAILS = "user_details"
    }
}

