package com.wk.fileupload.utils

import android.content.SharedPreferences
import javax.inject.Inject

/**
 * class to provide functions to access shared preference values
 */
class PrefUtils @Inject constructor(){

    @Inject
    lateinit var appPref:SharedPreferences

}