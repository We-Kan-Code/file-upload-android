package com.wk.fileupload.utils

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.os.Build
import android.util.Patterns
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat.getSystemService
import com.amazonaws.mobile.client.AWSMobileClient
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtilityOptions
import com.amazonaws.services.s3.AmazonS3Client
import com.wk.fileupload.BuildConfig
import com.wk.fileupload.R
import java.security.MessageDigest

class AppUtils{
    companion object{

        val S3_CHANNEL_ID = "upload_channel"
        val S3_NOTIFICATION_ID = 3000

        fun createAppNotification(context:Context,title:String,content:String):NotificationCompat.Builder{
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
                createNotificationChannel(context)
            }
            var builder = NotificationCompat.Builder(context, S3_CHANNEL_ID)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(title)
                .setContentText(content)
            return builder
        }
        lateinit var notificationManager: NotificationManager
        private fun createNotificationChannel(context:Context) {
            // Create the NotificationChannel, but only on API 26+ because
            // the NotificationChannel class is new and not in the support library
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                val name = context.getString(R.string.channel_name)
                val descriptionText = context.getString(R.string.channel_description)
                val importance = NotificationManager.IMPORTANCE_DEFAULT
                val channel = NotificationChannel(S3_CHANNEL_ID, name, importance).apply {
                    description = descriptionText
                }
                // Register the channel with the system
                notificationManager =
                    context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
                notificationManager.createNotificationChannel(channel)
            }
        }

        /**
         *
         * create transfer util
         */
        fun createTranferUtil(context:Context):TransferUtility{
            val options = TransferUtilityOptions()
            options.transferThreadPoolSize = 3
            return TransferUtility.builder()
                .context(context)
                .awsConfiguration(AWSMobileClient.getInstance().configuration)
                .s3Client(AmazonS3Client(AWSMobileClient.getInstance()))
                .defaultBucket(BuildConfig.BUCKET_NAME)
                .transferUtilityOptions(options)
                .build()
        }
    }
}

