# Change log

### All the changes to the project will be recorded in this file with respective version number

## v1.0

## Added

1. Libraries 
	* Android X
	* Lifecycle extension(viewmodel & livedata)
	* Dagger 2
	* Retrofit
	* Moshi
	* Testing framework(JUnit,Robolectic,Espresso,UIAutomator)
	* Coroutine
	* Glide
	* Amazon S3

1. Features Added
	* Single file upload in Activity.
	* Multiple file upload in Service.